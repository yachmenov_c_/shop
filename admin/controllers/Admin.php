<?php

/**
 * Created by PhpStorm.
 * User: yaroslav
 * Date: 11.01.16
 * Time: 17:57
 */
class Admin extends Controller {
    protected $users;

    public function __construct() {
        if (!CurrentUser::isLogged()) {
            $this->redirect(Urls::getRoute('Login'));
        } else {
            $this->users = new DBUsers;
            Page::$title = CurrentUser::getCategory() === 'user' ? __('Your Panel') : __('Configure Panel');
            Page::$layout = 'backend.xml';
            Page::$template = 'admin/parts/dynamic.phtml';
        }
    }

    public function index() {
        if (CurrentUser::getCategory() === 'admin') {
            $productsStock = new DBProducts;
            $products = $productsStock->getList(['order_by' => 'price']);
            foreach ($products as &$product)
                $product['category'] = $productsStock->getCategoryName($product['category_id']);
            $table = new VisualTable(
                $products,
                array(
                    'id'       => __('Id'),
                    'name'     => __('Name'),
                    'category' => __('Category of product'),
                    'in_stock' => __('Now in stock'),
                    'price'    => __('Price in') . '<b style="color:green;"> $</b>'),
                array(), array('id'),
                array('changeable'         => true,
                      'changeable_columns' => array('name', 'price', 'in_stock'),
                      'changeable_url'     => Urls::getRoute('admin', 'AdminAjax', 'quickChangeProducts'),
                      'tooltip'            => true
                )
            );
            return $this->view(array(
                'data' => array('VisualTable' => $table)
            ));
        } else {
            return $this->view(array('data' => __('Now we have not special options for your current user group, sorry...')));
        }
    }

    /**
     * Can be used only in __construct()
     */
    protected function noAdminProtection() {
        if (!CurrentUser::isAdmin()) {
            self::setActionName('page404');
            return true;
        }
        return false;
    }
}
<?php

/**
 * Created by PhpStorm.
 * User: yaroslav
 * Date: 04.02.16
 * Time: 14:32
 */
class ManageProducts extends Admin {
    protected $productsStock;
    protected $categoriesStock;

    public function __construct() {
        parent::__construct();
        if ($this->noAdminProtection()) return;

        Page::$title = __('Manage Products');
        $this->productsStock = new DBProducts;
        $this->categoriesStock = new DBProductsCategories;
    }

    public function index() {
        $products = $this->productsStock->getList(['order_by' => 'price']);
        foreach ($products as &$product) {
            $product['category'] = $this->productsStock->getCategoryName($product['category_id']);
            $product['delete'] = Urls::getRoute('admin', __CLASS__, 'delete', ['id' => $product['id']]);
            $product['edit'] = Urls::getRoute('admin', __CLASS__, 'edit', ['id' => $product['id']]);
            $product['image'] = sprintf("<img class='img-responsive img-thumbnail inline-table' src='%sproducts/%s'>", URL_IMG, $product['image']);
        }
        $table = new VisualTable(
            $products,
            array(
                'id'       => '',
                'edit'     => __('Edit'),
                'delete'   => __('Delete'),
                'name'     => __('Name'),
                'category' => __('Category of product'),
                'in_stock' => __('Now in stock'),
                'price'    => __('Price in') . '<b style="color:green;"> $</b>',
                'image'    => __('Image')
            ),
            array('edit', 'delete'), array('id'),
            array(
                'changeable'     => true, 'changeable_columns' => array('name', 'in_stock', 'price'),
                'changeable_url' => Urls::getRoute('admin', 'AdminAjax', 'quickChangeProducts')
            )
        );
        return $this->view(array(
            'data' => array('VisualTable' => $table)
        ));
    }

    public function delete($id) {
        if ($this->productsStock->get($id))
            $this->productsStock->delete($id);
        $this->redirect(Urls::getRoute('admin', __CLASS__));
    }

    public function edit($id) {
        if (!empty($id)) {
            if (!empty($this->productsStock->get($id))) {
                $success = null;
                if (isset($_POST['name']) && isset($_POST['description']) && isset($_POST['category_id'])) {
                    $data = $_POST;
                    $data['id'] = $id;
                    $this->productsStock->update($data);
                    $success = true;
                }
                $product = $this->productsStock->get($id);
                $product['categories'] = $this->categoriesStock->getList(['order_by' => 'name']);
                return $this->view(array(
                    'title' => Page::$title . ": {$product['name']}",
                    'data'  => array(
                        'product'      => $product,
                        'status_class' => $success ? 'panel-success' : ''
                    ),
                    'name'  => 'admin/editProduct.phtml'
                ));
            }
        }
        return $this->page404();
    }
}
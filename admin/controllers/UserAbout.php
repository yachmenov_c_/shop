<?php

/**
 * Created by PhpStorm.
 * User: yaroslav
 * Date: 06.02.16
 * Time: 17:40
 */
class UserAbout extends Admin {
    public function __construct() {
        parent::__construct();
        Page::$title = __('About me');
    }

    public function index() {
        $user_id = CurrentUser::getId();

        if(!empty($_POST)){
            if($this->checkPostData($key = 'name')) {
                $this->users->updateName($user_id, $_POST[$key]);
                $_SESSION['user']['name'] = $_POST[$key];
            }

            if($this->checkPostData($key = 'email'))
                $this->users->updateEmail($user_id, $_POST[$key]);

            if($this->checkPostData($key = 'password'))
                $this->users->updatePassword($user_id, $_POST[$key]);

            if($this->checkPostData($key = 'language')) {
                DBWorker::setUserMeta($user_id, $key, $_POST[$key], false);
                load_default_textdomain($_POST[$key]);
            }

            if($this->checkPostData($key = 'shipping_name'))
                DBWorker::setUserMeta($user_id, $key, $_POST[$key], false);

            if($this->checkPostData($key = 'shipping_street'))
                DBWorker::setUserMeta($user_id, $key, $_POST[$key], false);

            if($this->checkPostData($key = 'shipping_city'))
                DBWorker::setUserMeta($user_id, $key, $_POST[$key], false);

            if($this->checkPostData($key = 'shipping_state'))
                DBWorker::setUserMeta($user_id, $key, $_POST[$key], false);

            if($this->checkPostData($key = 'shipping_country'))
                DBWorker::setUserMeta($user_id, $key, $_POST[$key], false);

            if($this->checkPostData($key = 'shipping_zip'))
                DBWorker::setUserMeta($user_id, $key, $_POST[$key], false);

            if($this->checkPostData($key = 'shipping_phone'))
                DBWorker::setUserMeta($user_id, $key, $_POST[$key], false);
        }
        $user = $this->users->get($user_id);
        $user['category_name'] = $this->users->getCategoryName($user_id);
        $user['languages'] = explode(',', DBWorker::getOption('available_languages', false));
        $user['countries'] = explode(',', DBWorker::getOption('available_countries', false));
        $user['user_language'] = DBWorker::getUserMeta($user_id, 'language', false);

        $user['shipping_name'] = DBWorker::getUserMeta($user_id, 'shipping_name', false);
        $user['shipping_street'] = DBWorker::getUserMeta($user_id, 'shipping_street', false);
        $user['shipping_city'] = DBWorker::getUserMeta($user_id, 'shipping_city', false);
        $user['shipping_state'] = DBWorker::getUserMeta($user_id, 'shipping_state', false);
        $user['shipping_country'] = DBWorker::getUserMeta($user_id, 'shipping_country', false);
        $user['shipping_zip'] = DBWorker::getUserMeta($user_id, 'shipping_zip', false);
        $user['shipping_phone'] = DBWorker::getUserMeta($user_id, 'shipping_phone', false);

        return $this->view(array(
            'data' => $user
        ));
    }

    protected function checkPostData($key){
        return isset($_POST[$key]) && !empty($_POST[$key]);
    }
}
<?php

/**
 * Created by PhpStorm.
 * User: yaroslav
 * Date: 06.02.16
 * Time: 17:41
 */
class AdminAjax extends Admin {
    public function index() {
        echo false;
    }

    public function panelVisited() {
        if (isset($_POST['visited']))
            $_SESSION['user']['admin_panel_visited'] = $_POST['visited'];
    }

    public function quickChangeProducts() {
        $this->quickChange('products');
    }

    public function quickChangeProductsCategories() {
        $this->quickChange('products_categories');
    }

    private function quickChange($table_name) {
        if (isset($_POST['id']) && isset($_POST['key']) && isset($_POST['data_field'])) {
            $id = (int)$_POST['id'];
            $key = htmlentities($_POST['key']);
            $data_field = htmlentities($_POST['data_field']);
            $sql = "UPDATE {$table_name} SET {$key} = ? WHERE `id` = {$id}";
            $res = DBWorker::instance()->pdo->prepare($sql);
            if (is_numeric($data_field)) {
                if (is_real($data_field))
                    $res->bindValue(1, (float)$data_field, PDO::PARAM_INT);
                else $res->bindValue(1, (int)$data_field, PDO::PARAM_INT);
            } else $res->bindValue(1, $data_field, PDO::PARAM_STR);
            echo $res->execute();
        }
    }
}
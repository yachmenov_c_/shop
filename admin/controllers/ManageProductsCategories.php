<?php

/**
 * Created by PhpStorm.
 * User: yaroslav
 * Date: 04.02.16
 * Time: 14:32
 */
class ManageProductsCategories extends Admin {
    public $categoriesStock;
    public $productsStock;

    public function __construct() {
        parent::__construct();
        if ($this->noAdminProtection()) return;

        Page::$title = __('Manage Products Categories');
        $this->productsStock = new DBProducts;
        $this->categoriesStock = new DBProductsCategories;
    }

    public function index() {
        $categories = $this->categoriesStock->getList(['order_by' => 'name']);
        foreach ($categories as &$category) {
            $category['count'] = '<span class="badge">' . $this->productsStock->count($category['id']) . '</span>';
            $category['delete'] = Urls::getRoute('admin', __CLASS__, 'delete', ['id' => $category['id']]);
            $category['edit'] = Urls::getRoute('admin', __CLASS__, 'edit', ['id' => $category['id']]);
            $category['image'] = sprintf("<img class='img-responsive img-thumbnail inline-table' src='%scategories/%s'>", URL_IMG, $category['image']);
        }
        $table = new VisualTable(
            $categories,
            array(
                'id'     => '',
                'edit'   => __('Edit'),
                'delete' => __('Delete'),
                'name'   => __('Name'),
                'count'  => __('Products in category'),
                'image'  => __('Image')
            ),
            array('edit', 'delete'), array('id'),
            array('changeable'     => true, 'changeable_columns' => array('name'),
                  'changeable_url' => Urls::getRoute('admin', 'AdminAjax', 'quickChangeProductsCategories'))
        );
        return $this->view(array(
            'data' => array('VisualTable' => $table)
        ));
    }

    public function delete($id) {
        if ($this->categoriesStock->get($id))
            $this->categoriesStock->delete($id);
        $this->redirect(Urls::getRoute('admin', __CLASS__));
    }

    public function edit($id) {
        if (!empty($id)) {
            if (!empty($this->categoriesStock->get($id))) {
                if (isset($_POST['name']))
                    $this->categoriesStock->update(['id' => $id, 'name' => $_POST['name']]);
                $category = $this->categoriesStock->get($id);
                return $this->view(array(
                    'title' => Page::$title . ": {$category['name']}",
                    'data'  => $category,
                    'name'  => 'admin/editProductCategory.phtml'
                ));
            }
        }
        return $this->page404();
    }
}
<?php

/**
 * Created by PhpStorm.
 * User: yaroslav
 * Date: 03.02.16
 * Time: 11:02
 */
class VisualTable {
    public $items;
    public $columnHeaders;
    public $columnHiddenKeys;
    public $settings;

    const dirTemplate = 'visual-table';

    public function __construct(array $table_data = array(),
                                array $columns = array(),
                                array $action_columns = array(),
                                array $hidden_columns = array(),
                                array $settings = array('changeable'         => false,
                                                        'changeable_columns' => array(),
                                                        'changeable_url'     => '#',
                                                        'tooltip'            => true
                                )
    ) {
        $this->items = $table_data;
        $this->columnHeaders = array_values($columns);
        $this->columnHiddenKeys = $hidden_columns;
        if (!empty($columns)) {
            for ($i = 0; $i < count($table_data); $i++) {
                $new_order = array();
                while (list($key,) = each($columns))
                    $new_order[$key] = in_array($key, $action_columns) ?
                        $this->manageActions($key, $this->items[$i][$key])
                        : $this->items[$i][$key];
                reset($columns);
                $this->items[$i] = $new_order;
            }
        }
        $this->setSettings($settings, $columns);
        if (!empty($action_columns)) $this->settings['actionable'] = true;
        else $this->settings['actionable'] = false;
    }

    protected function manageActions($key, $action) {
        $extra_class = '';
        switch ($key) {
            case 'delete':
                $extra_class = 'visual-action-delete';
                break;
        }
        return sprintf('<a href="%s" class="visual-action-normal %s">%s</a>', $action, $extra_class, strtolower($key));
    }

    protected function setSettings($settings, $columns) {
        $this->settings = $settings;
        if (!isset($this->settings['changeable']))
            $this->settings['changeable'] = false;
        elseif (!isset($this->settings['changeable_columns']))
            $this->settings['changeable_columns'] = array();
        if ((!isset($columns['id']) && !isset($columns['Id']) && !isset($columns['ID'])) && $this->settings['changeable'])
            throw new Exception(__CLASS__ . ' requires \'id\' column for changeable options!');
        if (!isset($this->settings['changeable_url']) && $this->settings['changeable'])
            throw new Exception(__CLASS__ . ' requires \'changeable_url\' string for changeable request!');
        if (!isset($this->settings['tooltip']))
            $this->settings['tooltip'] = false;
    }

    public function display() {
        ob_start();
        $this->sectionActions();
        $this->sectionChangeable();
        ?>
        <div class="table-responsive visual-table
         <?php $this->settings['changeable'] ? print('visual-changeable') : print(''); ?>">
            <table <?php if ($this->settings['tooltip'] && $this->settings['changeable'])
                echo 'data-toggle="tooltip" data-placement="bottom"
                title="', __('Just make dbclick on shadow territories in the table!'), '" id="visual-tooltip"'; ?>
                class="table table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <?php $this->displayRow($this->columnHeaders) ?>
                </tr>
                </thead>
                <tbody>
                <?php for ($i = 0; $i < count($this->items); $i++): ?>
                    <tr>
                        <td class='visual-count'><?php printf('%d)', $i + 1) ?></td>
                        <?php $this->displayRow($this->items[$i]) ?>
                    </tr>
                <?php endfor; ?>
                </tbody>
            </table>
        </div>
        <?php print(ob_get_clean());
    }

    public function displayRow($data) {
        ob_start(); ?>
        <?php
        foreach ($data as $key => $d) {
            ?>
            <td style="<?php if (in_array($key, $this->columnHiddenKeys)) print('display:none;'); ?>"
                <?php
                if (($this->settings['changeable'] && in_array($key, $this->settings['changeable_columns'])))
                    printf('class="visual-td-changeable" visual-changeable-data="%s"', $key);
                elseif (!substr_compare($key, 'id', 0)) printf('visual-requires-data="%s"', $key);
                ?>
            ><?= $d ?></td>
            <?php
        }
        print(ob_get_clean());
    }

    protected function sectionChangeable() {
        ob_start();
        if ($this->settings['changeable']):
            ?>
            <script>
                jQuery.fn.extend({
                    visual_changeable_url: function () {
                        return "<?php echo $this->settings['changeable_url'] ?>";
                    }
                });
            </script>
            <?php
            Page::includeFile(self::dirTemplate . '/changeable.phtml');
        endif;
        print(ob_get_clean());
    }

    protected function sectionActions() {
        ob_start();
        if ($this->settings['actionable'])
            Page::includeFile(self::dirTemplate . '/modal_delete.phtml');
        print(ob_get_clean());
    }
}
<?php

/**
 * Created by PhpStorm.
 * User: yaroslav
 * Date: 02.01.16
 * Time: 23:51
 */
class RouterConfig extends UrlWorker {

    public static $readyRoutes = array();

    public function registerRoutes(array $collection = null) {
        if (empty($collection)) $collection = array();

        $collection['default'] = array(
            'url'      => 'home',
            'defaults' => ['controller' => 'Home', 'action' => self::mainAction]
        );

        $collection['admin'] = array(
            'url'      => 'yyo-admin/{controller}/{action}',
            'defaults' => $this->getDynamicDefForAdmin(self::getParamValue(1), self::getParamValue(2), ['Admin', self::mainAction])
        );

        return $collection;
    }

    protected function getDynamicDefForAdmin($class, $method, $defaults) {
        $result = $this->caseSearchControllerAndAction($class, $method, function ($path) {
            return strpos($path, __ROOT__ . 'admin') !== false ? true : false;
        });

        if ($result['controller'] === null) list($result['controller'], $result['action']) = $defaults;
        return $result;
    }
}
<?php

/**
 * Created by PhpStorm.
 * User: yaroslav
 * Date: 08.01.16
 * Time: 10:47
 */
class App extends UrlWorker {

    public static function run() {
        Router::instance()->setRoute();
        $class_name = self::getControllerName();
        $object = new $class_name;
        if (!self::$protectionRedirect)
            print(App::controllerMethod($object, self::getActionName()));
    }

    private static function controllerMethod($class, $method) {
        $reflection = new ReflectionMethod($class, $method);
        $set_args = array();
        foreach ($reflection->getParameters() as $arg)
            $set_args[] = self::getParamValue(strtolower($arg->name));
        return call_user_func_array(array($class, $method), $set_args);
    }

}
<?php

/**
 * Created by PhpStorm.
 * User: yaroslav
 * Date: 02.01.16
 * Time: 23:28
 */
abstract class Controller extends UrlWorker {
    const page404 = 'Page404';

    abstract function index();

    protected function view($page = null) {
        if (!empty($page)) {
            if (isset($page['name']) || array_key_exists('name', $page)) Page::$name = $page['name'];
            elseif (empty(Page::$name)) {
                $called_class = self::getControllerName();
                $called_method = self::getActionName();
                Page::$name = !empty($called_method) ? $called_method . '.phtml' : $called_class . '.phtml';
                if (!empty($files = SearchWorker::rGlob(__ROOT__ . Page::dirTemplates . Page::$name)))
                    Page::$name = $files[0];
                else {
                    Page::$name = SearchWorker::rGlob(__ROOT__ . Page::dirTemplates . $called_class . '.phtml');
                    if (isset(Page::$name[0])) Page::$name = Page::$name[0];
                }
            }
            if (isset($page['title']) || array_key_exists('title', $page))
                Page::$title = $page['title'];
            if (isset($page['data']) || array_key_exists('data', $page))
                Page::$data = $page['data'];
            if (isset($page['layout']) || array_key_exists('layout', $page))
                Page::$layout = $page['layout'];
            if (isset($page['template']) || array_key_exists('template', $page))
                Page::$template = $page['template'];
        }
        return Page::build();
    }

    protected function redirect($url) {
        if (!headers_sent($filename, $line_num)) {
            self::$protectionRedirect = true;
            header("Location: " . $url);
        } else {
            throw new Exception("Headers have been already sent in $filename in line $line_num!");
        }
    }

    public function page404() {
        /** @var $page404Object Page404 */
        $class_name = self::page404;
        $page404Object = new $class_name;
        return $page404Object->index();
    }

    protected function json($data) {
        return json_encode($data);
    }

}
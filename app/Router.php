<?php

/**
 * Created by PhpStorm.
 * User: yaroslav
 * Date: 02.01.16
 * Time: 13:45
 */
class Router extends UrlWorker implements ISingleton {

    protected static $instance = null;
    protected $className;
    protected $classMethodName;
    protected $routerConfig;
    protected $urlParts;

    protected function __construct() {
        $local_parts = $this->urlParts = array_slice(explode('/', $_SERVER['REQUEST_URI']), 1);
        array_walk($local_parts, function (&$el) {
            $el = strtolower($el);
        });
        $this->urlParts = $local_parts;

        $this->routerConfig = new RouterConfig();
        RouterConfig::$readyRoutes = $this->routerConfig->registerRoutes();

        if (empty($this->urlParts[0])) {
            $this->className = RouterConfig::$readyRoutes['default']['defaults']['controller'];
            $this->classMethodName = RouterConfig::$readyRoutes['default']['defaults']['action'];
        } elseif (!$this->setConfig()) {
            $this->className = 'Page404';
            $this->classMethodName = self::mainAction;
        } elseif (!is_subclass_of($this->className, self::controllerName)) {
            throw new LogicException("Class ({$this->className}), which is configured in the RouterConfig for Controller hasn't it in parent's tree!");
        }
    }

    /**
     * @return bool
     */
    protected function setConfig() {

        if (($routes_result = $this->checkForRoutes()) !== null) {
            return $routes_result;
        } else {

            if (!isset($this->urlParts[1])) $this->urlParts[1] = null;

            list($this->className, $this->classMethodName) =
                array_values($this->caseSearchControllerAndAction($this->urlParts[0], $this->urlParts[1], function ($path) {
                    return strpos($path, __ROOT__ . 'controllers') !== false ? true : false;
                }));

            if (!$this->emptyControllerAndAction()) return true;
        }

        return false;
    }

    /**
     * @return bool|null
     */
    protected function checkForRoutes() {

        $was_searched = false;
        $route = null;
        foreach (RouterConfig::$readyRoutes as $key => $route) {

            $conf_url = strtolower(self::getUnitedUrl($route));
            $curr_url = strtolower(self::getCurrentUrl());

            if ($conf_url === $curr_url || $conf_url === $curr_url . '/') {
                $was_searched = true;
                break;
            } elseif (($controller_pos = array_search(strtolower($route['defaults']['controller']), $this->urlParts, true)) !== false) {
                $exploded = explode('/', $route['url']);
                if (isset($exploded[$controller_pos]) && $exploded[$controller_pos] === '{controller}') {
                    $was_searched = true;
                    break;
                }

                return false;
            }

        }

        if ($was_searched && is_array($route)) {
            $this->className = $route['defaults']['controller'];
            $this->classMethodName = $route['defaults']['action'];
            if ($this->emptyControllerAndAction()) return false;

            return true;
        }

        return null;
    }

    protected function emptyControllerAndAction() {
        return (!empty($this->className) && !empty($this->classMethodName)) ? false : true;
    }

    /**
     * @return void
     */
    public function setRoute() {
        self::setControllerName($this->className);
        self::setActionName($this->classMethodName);
    }

    /**
     * @return Router
     */
    public static function instance() {
        if (self::$instance === null)
            self::$instance = new static;
        return self::$instance;
    }

    public function __clone() {
        $this->ownSingletonException('cloned');
    }

    public function __sleep() {
        $this->ownSingletonException('serialized');
    }

    public function __wakeup() {
        $this->ownSingletonException('unserialized');
    }

    /**
     * @param string $message
     * @throws LogicException
     */
    protected function ownSingletonException($message) {
        throw new LogicException("Singleton object of " . __CLASS__ . " class can't be {$message}, cheating?");
    }

}
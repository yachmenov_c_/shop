<?php
/**
 * Created by WordPress
 * User: WP, yaroslav
 * Date: 01.06.16
 * Time: 22:37
 *
 * Provides file-like methods for manipulating a string instead
 * of a physical file.
 *
 */
class POMOStringReader extends POMOReader {

    var $_str = '';

    /**
     * PHP5 constructor.
     */
    function __construct( $str = '' ) {
        parent::POMO_Reader();
        $this->_str = $str;
        $this->_pos = 0;
    }

    /**
     * PHP4 constructor.
     */
    public function POMO_StringReader( $str = '' ) {
        self::__construct( $str );
    }

    /**
     * @param string $bytes
     * @return string
     */
    function read($bytes) {
        $data = $this->substr($this->_str, $this->_pos, $bytes);
        $this->_pos += $bytes;
        if ($this->strlen($this->_str) < $this->_pos) $this->_pos = $this->strlen($this->_str);
        return $data;
    }

    /**
     * @param int $pos
     * @return int
     */
    function seekto($pos) {
        $this->_pos = $pos;
        if ($this->strlen($this->_str) < $this->_pos) $this->_pos = $this->strlen($this->_str);
        return $this->_pos;
    }

    /**
     * @return int
     */
    function length() {
        return $this->strlen($this->_str);
    }

    /**
     * @return string
     */
    function read_all() {
        return $this->substr($this->_str, $this->_pos, $this->strlen($this->_str));
    }

}
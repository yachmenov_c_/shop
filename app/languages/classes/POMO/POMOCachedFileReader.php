<?php
/**
 * Created by PhpStorm.
 * User: yaroslav
 * Date: 01.06.16
 * Time: 22:35
 *
 * Reads the contents of the file in the beginning.
 *
 */
class POMOCachedFileReader extends POMOStringReader {
    /**
     * PHP5 constructor.
     */
    function __construct( $filename ) {
        parent::POMO_StringReader();
        $this->_str = file_get_contents($filename);
        if (false === $this->_str)
            return false;
        $this->_pos = 0;
    }

    /**
     * PHP4 constructor.
     */
    public function POMO_CachedFileReader( $filename ) {
        self::__construct( $filename );
    }
}
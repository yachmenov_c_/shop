<?php
/**
 * Created by WordPress
 * User: WP, yaroslav
 * Date: 01.06.16
 * Time: 22:39
 */
class POMOFileReader extends POMOReader {

    /**
     * @param string $filename
     */
    function __construct( $filename ) {
        parent::POMO_Reader();
        $this->_f = fopen($filename, 'rb');
    }

    /**
     * PHP4 constructor.
     */
    public function POMO_FileReader( $filename ) {
        self::__construct( $filename );
    }

    /**
     * @param int $bytes
     */
    function read($bytes) {
        return fread($this->_f, $bytes);
    }

    /**
     * @param int $pos
     * @return boolean
     */
    function seekto($pos) {
        if ( -1 == fseek($this->_f, $pos, SEEK_SET)) {
            return false;
        }
        $this->_pos = $pos;
        return true;
    }

    /**
     * @return bool
     */
    function is_resource() {
        return is_resource($this->_f);
    }

    /**
     * @return bool
     */
    function feof() {
        return feof($this->_f);
    }

    /**
     * @return bool
     */
    function close() {
        return fclose($this->_f);
    }

    /**
     * @return string
     */
    function read_all() {
        $all = '';
        while ( !$this->feof() )
            $all .= $this->read(4096);
        return $all;
    }
}
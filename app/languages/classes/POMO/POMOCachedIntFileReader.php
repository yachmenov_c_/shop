<?php
/**
 * Created by WordPress
 * User: WP, yaroslav
 * Date: 01.06.16
 * Time: 22:34
 *
 * Reads the contents of the file in the beginning.
 *
 */
class POMOCachedIntFileReader extends POMOCachedFileReader {
    /**
     * PHP5 constructor.
     */
    public function __construct( $filename ) {
        parent::POMO_CachedFileReader($filename);
    }

    /**
     * PHP4 constructor.
     */
    function POMO_CachedIntFileReader( $filename ) {
        self::__construct( $filename );
    }
}
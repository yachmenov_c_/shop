<?php

/**
 * Created by PhpStorm.
 * User: yaroslav
 * Date: 08.01.16
 * Time: 14:55
 */
class Page {
    public static $template = '';
    public static $layout = 'frontend.xml';
    public static $name;
    public static $data;
    public static $title;

    const defTemplate = 'parts/dynamic.phtml';
    const dirTemplates = 'templates/';
    const dirLayouts = 'layouts/';

    public static function build() {
        $included_values = self::layoutXML();
        ob_clean();
        if (!empty($included_values)) {
            $last_index = count($included_values) - 1;
            for ($i = 0; $i <= $last_index; $i++) {
                switch ($i) {
                    case 0:
                        print('<! DOCTYPE html><html>');
                        self::includeFile($included_values[0]['path']);
                        print('<body>');
                        break;
                    case $last_index:
                        self::includeFile($included_values[$last_index]['path']);
                        print('</body></html>');
                        break;
                    default:
                        if (!strcmp($included_values[$i]['type'], 'dynamic'))
                            self::includeFile(self::$template ? self::$template : self::$template = self::defTemplate);
                        else self::includeFile($included_values[$i]['path']);
                        break;
                }
            }
        } else print('<div class="error-debug">Layout isn\'t founded or not working!</div>');
        return ob_get_clean();
    }

    public static function renderBody() {
        self::includeFile(self::$name);
    }

    public static function includeFile($path, $print_error = DEV_DISPLAY_ERRORS) {
        if ($path = (string)$path) {
            if (!file_exists($path)) $path = __ROOT__ . self::dirTemplates . $path;
            if (file_exists($path)) {
                include_once($path);
                return true;
            }
            if ($print_error)
                print("<div class='error-debug'>File or directory {$path} not found!</div>");
            trigger_error("File or directory {$path} not found!", E_USER_ERROR);
        }
        return false;
    }

    protected static function layoutXML() {
        $xml_path = __ROOT__ . Page::dirLayouts . Page::$layout;
        if (file_exists($xml_path)) {
            $xml = new SimpleXMLElement($xml_path, 0, true);
            $values = [];
            foreach ($xml->xpath(
                '/page/template[type = "static" or @type = "static"]
             | /page/template[type="dynamic" or @type = "dynamic"]'
            ) as $value) {
                $select_all = (array)$value;
                if (isset($select_all['@attributes'])) {
                    $select_all = array_merge($select_all['@attributes'], $select_all);
                    unset($select_all['@attributes']);
                }
                $values[] = $select_all;
            }
            return $values;
        }
        return false;
    }
}
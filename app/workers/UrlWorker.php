<?php

/**
 * Created by PhpStorm.
 * User: yaroslav
 * Date: 17.01.16
 * Time: 17:12
 */
abstract class UrlWorker {
    private static $actionClass = null;
    private static $actionClassMethod = null;

    protected static $protectionRedirect = false;

    const routerName = 'Router';
    const controllerName = 'Controller';
    const mainAction = 'index';

    /**
     * @param $param string
     * @return string|bool
     */
    protected static function getParamValue($param) {
        $url_slices = array_slice(explode('/', $_SERVER['REQUEST_URI']), 1);
        if (is_numeric($param) && $param < count($url_slices) && $param >= 0)
            return $url_slices[(int)$param];
        $key = array_search($param, $url_slices);
        if (count($url_slices) < $key + 2 || $key === false) return false;
        return $url_slices[$key + 1];
    }

    /**
     * @param $element array
     * @return string
     */
    protected static function getUnitedUrl($element) {
        $prefix = self::getUrlPrefix() . '/';

        if(!preg_match('~\{.+\}~', $element['url'])) {
            return $prefix . $element['url'];
        }

        foreach($element['defaults'] as $key => $value){
            $element['url'] = str_replace('{'.$key.'}', $value, $element['url']);
        }

        return $prefix . $element['url'];
    }

    protected static function getUrlPrefix() {
        return ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') ? 'https' : 'http') .
        '://' . $_SERVER['HTTP_HOST'];
    }

    protected static function getCurrentUrl() {
        return self::getUrlPrefix() . $_SERVER['REQUEST_URI'];
    }

    protected static function getControllerName() {
        return self::$actionClass;
    }

    protected static function getActionName() {
        return self::$actionClassMethod;
    }

    protected static function setControllerName($name) {
        if (self::routerName === static::class) {
            self::$actionClass = $name;
        } else {
            throw new LogicException(self::getLogicErrorMessage(__METHOD__, self::routerName));
        }
    }

    protected static function setActionName($name) {
        if (is_subclass_of(static::class, self::controllerName) || self::routerName === static::class) {
            self::$actionClassMethod = $name;
        } else {
            throw new LogicException(self::getLogicErrorMessage(__METHOD__, self::controllerName));
        }
    }

    private static function getLogicErrorMessage($method_name, $className) {
        return $method_name . ' can be used only by ' . $className . ' class instance! (according to YYO MVC logic)';
    }

    /**
     * @param $class_for_search string
     * @param $method_for_search string
     * @param $path_callback callable
     * @return array
     */
    public function caseSearchControllerAndAction($class_for_search, $method_for_search, $path_callback) {
        $searched_class = null;
        $searched_method = null;
        foreach (SearchWorker::$dbClasses as $key => $path) {
            if (!strcasecmp($class_for_search, $key) && $path_callback($path)) {
                $searched_class = $key;
                if (empty($method_for_search)) {
                    $searched_method = self::mainAction;
                    break;
                } else {
                    $class_methods = get_class_methods($searched_class);
                    foreach ($class_methods as $method_name) {
                        if (!strcasecmp($method_for_search, $method_name)) {
                            $reflection = new ReflectionMethod($searched_class, $method_name);
                            if ($reflection->isPublic()){
                                $searched_method = $method_name;
                            }

                            /* There must be exception */
                            break;
                        }
                    }
                }
                break;
            }
        }
        return array('controller' => $searched_class, 'action' => $searched_method);
    }

    /**
     * @param string $action
     * @return bool
     */
    public static function isMainAction($action){
        return $action === self::mainAction;
    }

}
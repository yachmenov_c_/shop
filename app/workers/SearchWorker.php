<?php

/**
 * Created by PhpStorm.
 * User: yaroslav
 * Date: 16.01.16
 * Time: 19:17
 */
class SearchWorker {
    public static $dbClasses = array();

    public static function rGlob($pattern, $flags = null, $recursive = true) {
        $files = glob($pattern, $flags);
        if (!$recursive) return $files;
        foreach (glob(dirname($pattern) . '/*', GLOB_ONLYDIR | GLOB_NOSORT) as $dir) {
            $files = array_merge($files, self::rGlob($dir . DIRECTORY_SEPARATOR . basename($pattern), $flags));
        }
        return $files;
    }
}
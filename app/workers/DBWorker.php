<?php

/**
 * Created by PhpStorm.
 * User: yaroslav
 * Date: 01.06.16
 * Time: 0:35
 *
 * @property PDO $pdo
 */
class DBWorker extends Connect implements INoSingleton{

    private static $instance = null;

    public function __construct() {
        self::$instance = $this;
        $this->setConfig();
    }

    public static function getFunc($func_name, ...$params) {
        $sql = "SELECT {$func_name}(?";
        $start_index = 0;
        if(count($params) === 2){
            $start_index++;
            $sql .= ', ?)';
            $res = self::getPDO()->prepare($sql);
            $res->bindParam(1, $params[0], PDO::PARAM_INT);
        } else {
            $sql .= ')';
            $res = self::getPDO()->prepare($sql);
        }
        $res->bindParam($start_index+1, $params[$start_index], PDO::PARAM_STR, 255);
        return $res->execute() ? $res->fetch(PDO::FETCH_NUM)[0] : false;
    }

    public static function setFunc($procedure_name, ...$params) {
        $sql = "CALL {$procedure_name}(?, ?";
        $start_index = 0;
        if(count($params) === 3){
            $start_index++;
            $sql .= ', ?)';
            $res = self::getPDO()->prepare($sql);
            $res->bindParam(1, $params[0], PDO::PARAM_INT);
        } else {
            $sql .= ')';
            $res = self::getPDO()->prepare($sql);
        }
        $res->bindParam($start_index+1, $params[$start_index], PDO::PARAM_STR, 255);
        $res->bindParam($start_index+2, $params[$start_index+1], PDO::PARAM_STR, 4000);
        return $res->execute();
    }

    public static function getOption($op_name, $will_un_serialize = true){
        $result = self::getFunc('get_option', $op_name);
        return $will_un_serialize ? unserialize($result) : $result;
    }

    public static function setOption($op_name, $op_value, $will_serialize = true){
        self::setFunc('update_option', $op_name, $will_serialize ? serialize($op_value) : $op_value );
    }

    public static function getUserMeta($user_id, $op_name, $will_un_serialize = true){
        $result = self::getFunc('get_user_meta', $user_id, $op_name);
        return $will_un_serialize ? unserialize($result) : $result;
    }

    public static function setUserMeta($user_id, $op_name, $op_value, $will_serialize = true){
        self::setFunc('update_user_meta', $user_id, $op_name, $will_serialize ? serialize($op_value) : $op_value );
    }

    public function __get($name){
        switch($name){
            case 'pdo':
                return parent::getPDO();
        }

        return null;
    }

    public function __set($name, $value){
        switch($name){
            case 'pdo':
                throw new LogicException('You can not override PDO object, only one instance! It is in the reasons for quick and one access.');
                break;
        }
    }


    public function setConfig() {
        parent::setConfig();
        $this->config['password'] = md5($this->config['password']);
    }

    /* Changed logic from Singleton to so-so normal class */

    public function run(){}

    public function __clone() {
        return clone $this;
    }

    /**
     * @return DBWorker|null
     */
    public static function instance() {
        return self::$instance;
    }

    public function __sleep() {}

    public function __wakeup() {}

}
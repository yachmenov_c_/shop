<?php

/**
 * Created by PhpStorm.
 * User: yaroslav
 * Date: 06.02.16
 * Time: 20:40
 */
abstract class SingletonWorker implements ISingleton{
    private static $instances = array();

    protected function __construct() {}

    /**
     * @return SingletonWorker - child instance
     */
    public static function instance() {
        $class = static::class;
        if (array_key_exists($class, self::$instances) === false)
            self::$instances[$class] = new static;
        return self::$instances[$class];
    }

    abstract public function run();

    public function __clone() {
        throw new LogicException("Singleton family objects can't be cloned");
    }

    public function __sleep() {
        throw new LogicException("Singleton family objects can't be serialized");
    }

    public function __wakeup() {
        throw new LogicException("Singleton family objects can't be unserialized");
    }
}
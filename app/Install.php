<?php

/**
 * Created by PhpStorm.
 * User: yaroslav
 * Date: 02.01.16
 * Time: 16:13
 */
class Install extends SingletonWorker {

    protected $dbName;

    protected $sqlFiles;
    protected $sqlFilesInstalled;
    /**
     * @var DBWorker
     */
    protected $DBWorker;

    public function run() {

        $connect = Connect::instance()->run();
        $this->DBWorker = new DBWorker();

        if ($connect->isNewDB || !$this->isOptionsExist()) {
            session_destroy();
            $this->runSQLFromPath(DIR_BOOTSTRAP . 'sql/options.sql');
        }

        if (!is_array($this->sqlFilesInstalled = DBWorker::getOption('installed_files'))) {
            $this->sqlFilesInstalled[] = DIR_BOOTSTRAP . 'sql/options.sql';
            if($this->installSqlFiles())
                DBWorker::setOption('installed_files', $this->sqlFilesInstalled);
        }

    }

    /**
     * @param string $sql
     * @deprecated
     * @throws Exception
     * @return PDOStatement | false
     */
    protected function query($sql = '') {
        try {
            $result = $this->DBWorker->pdo->query($sql);
        } catch (PDOException $e) {
            throw new Exception("Error!: " . $e->getMessage());
        }
        return $result;
    }

    protected function installSqlFiles() {
        $filesInstalled = $this->sqlFilesInstalled;

        $this->sqlFiles = SearchWorker::rGlob(DIR_BOOTSTRAP . 'sql/*.sql');
        $this->sqlFiles = array_filter($this->sqlFiles, function ($val) use ($filesInstalled) {
            return !in_array($val, $filesInstalled);
        });

        usort($this->sqlFiles, function ($a, $b) {
            $matches_a = $matches_b = array();
            $res_a = preg_match($pattern = '/_[[:digit:]]+$/', basename($a, $suf = '.sql'), $matches_a);
            $res_b = preg_match($pattern, basename($b, $suf), $matches_b);

            if ($res_a && $res_b) {
                $matches_a[0][0] = 0;
                $matches_b[0][0] = 0;
                $a = (int)$matches_a[0];
                $b = (int)$matches_b[0];
                if ($a === $b) {
                    return 0;
                }

                return $a < $b ? -1 : 1;
            }

            return !$res_a ? -1 : 1;
        });

        try {
            foreach ($this->sqlFiles as $file) {
                $this->runSQLFromPath($file);
                $this->sqlFilesInstalled[] = $file;
            }
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

    protected function isOptionsExist() {
        try {
            $this->DBWorker->pdo->query('SELECT 1 FROM  `options`;');
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }

    protected function runSQLFromPath($install_filename) {
        if (!file_exists($install_filename))
            throw new Exception('Error of opening ' . $install_filename . ' for creating default sql script!');
        $big_query = file_get_contents($install_filename);
        $this->query($big_query);
    }

}
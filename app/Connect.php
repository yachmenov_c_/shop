<?php

/**
 * Created by PhpStorm.
 * User: yaroslav
 * Date: 18.12.15
 * Time: 2:22
 *
 */
class Connect extends SingletonWorker {

    const workerName = 'DBWorker';
    private static $pdo;

    public $isNewDB = false;
    protected $config;

    public function run() {
        if (self::$pdo === null) {
            $this->setConfig();
            $config['password'] = md5($this->config['password']);
            try {
                self::$pdo = new PDO(
                    sprintf("mysql:host=%s", $this->config['server']),
                    $this->config['user'],
                    $this->config['password']
                );
                self::$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $result = self::$pdo->prepare("SHOW DATABASES LIKE ?");
                $result->bindValue(1, $this->config['dbName']);
                $result->execute();
                if (empty($result->fetch(PDO::FETCH_NUM)[0])) {
                    $sql = 'CREATE DATABASE IF NOT EXISTS ' . $this->config['dbName'];
                    self::$pdo->exec($sql);
                    $this->isNewDB = true;
                }
                self::$pdo->exec('USE ' . $this->config['dbName']);
            } catch (PDOException $e) {
                throw new Exception("Error!: " . $e->getMessage());
            }

            $this->config['password'] = $config['password'];

            return $this;

        } else throw new LogicException('Singleton method: can not do run() twice or more!');
    }

    protected function setConfig(){
        global $config;
        $this->config = $config;
    }

    public function getDBName() {
        return $this->config['dbName'];
    }

    public function getPassword() {
        return $this->config['password'];
    }

    /**
     * @return PDO
     */
    protected static function getPDO(){
        return self::$pdo;
    }

    /**
     * @return Connect
     */
    public static function instance() {
        return parent::instance();
    }

}
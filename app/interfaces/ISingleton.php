<?php

/**
 * Created by PhpStorm.
 * User: yaroslav
 * Date: 30.05.16
 * Time: 1:19
 */
interface ISingleton {

    static function instance();

    function __clone();

    function __sleep();

    function __wakeup();

}
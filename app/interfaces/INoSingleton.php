<?php

/**
 * Created by PhpStorm.
 * User: yaroslav
 * Date: 01.06.16
 * Time: 9:16
 */
interface INoSingleton {

    function __construct();

}
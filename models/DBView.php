<?php

/**
 * Created by PhpStorm.
 * User: yaroslav
 * Date: 30.05.16
 * Time: 4:22
 */
abstract class DBView extends DBWorker implements IDBModel{

    protected $table;

    public function __construct() {
        parent::__construct();
    }

    public function count() {
        $sql = "SELECT COUNT(*) FROM ";
        if (count($args = func_get_args())) {
            $table = $args[0];
            $sql .= "$table;";
        } elseif (isset($this->table))
            $sql .= "$this->table;";
        else return false;
        $res = $this->pdo->prepare($sql);
        return $res->execute() ? $res->fetch(PDO::FETCH_NUM)[0] : 0;
    }

    public function countColumns() {
        $sql = "SHOW COLUMNS FROM $this->table";
        $res = $this->pdo->prepare($sql);
        return $res->execute() ? count($res->fetchAll(PDO::FETCH_NUM)) : false;
    }

    public function get($id) {
        $sql = "SELECT * FROM {$this->table} WHERE `id` = :id;";
        $res = $this->pdo->prepare($sql);
        $res->bindValue(':id', $id, PDO::PARAM_INT);
        return $res->execute() ? $res->fetch(PDO::FETCH_ASSOC) : false;
    }

    public function delete($id) {
        $sql = "DELETE FROM {$this->table} WHERE `id` = :id;";
        $res = $this->pdo->prepare($sql);
        $res->bindValue(':id', $id, PDO::PARAM_INT);
        return  $res->execute() ? $res->rowCount() : false;
    }

}
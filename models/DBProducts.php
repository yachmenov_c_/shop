<?php

/**
 * Class DBProducts
 * @property int inStock
 */
class DBProducts extends DBTableCategories {
    protected $table = 'products';
    protected $tableOfCategories = 'products_categories';

    /**
     * @param int
     * @return int
     */
    public function count() {
        if (func_num_args() >= 1)
            $id = (int)func_get_arg(0);
        if (empty($id))
            return parent::count();
        else {
            $sql = "SELECT COUNT(*) FROM {$this->table}
                    WHERE :cat_id = `category_id`";
            $res = $this->pdo->prepare($sql);
            $res->bindValue(":cat_id", $id, PDO::PARAM_INT);
            $res->execute();
            return $res->fetch(PDO::FETCH_NUM)[0];
        }
    }

    public function getCategoryName($category_id) {
        $sql = "SELECT `name` FROM {$this->tableOfCategories}
                    WHERE :cat_id = `id`";
        $res = $this->pdo->prepare($sql);
        $res->bindValue(":cat_id", $category_id, PDO::PARAM_INT);
        return $res->execute() ? $res->fetch(PDO::FETCH_NUM)[0] : false;
    }

    public function getId($id) {
        return parent::getId(array('value' => $id));
    }

    public function update($data) {
        if (!empty($data)) {
            $sql = "UPDATE {$this->table} SET `name` = ?, `description` = ?, `category_id` = ?, `price` = ?
                    WHERE `id` = ?";
            $res = $this->pdo->prepare($sql);
            $res->bindValue(1, $data['name'], PDO::PARAM_STR);
            $res->bindValue(2, $data['description'], PDO::PARAM_STR);
            $res->bindValue(3, $data['category_id'], PDO::PARAM_INT);
            $res->bindValue(4, $data['price']);
            $res->bindValue(5, $data['id'], PDO::PARAM_INT);
            return $res->execute();
        }
        return false;
    }

    public function decStock($product_id){
        return $this->shiftStock($product_id, -1);
    }

    public function incStock($product_id){
        return $this->shiftStock($product_id, 1);
    }

    /**
     * @param int $product_id
     * @param int $shift
     * @return bool
     */
    public function shiftStock($product_id, $shift){
        $sql = "UPDATE {$this->table} SET `in_stock` = `in_stock` + ? WHERE `id` = ?";
        $res = $this->pdo->prepare($sql);
        $res->bindValue(1, $shift, PDO::PARAM_INT);
        $res->bindValue(2, $product_id, PDO::PARAM_INT);
        return $res->execute();
    }
}
<?php

/**
 * Created by PhpStorm.
 * User: yaroslav
 * Date: 30.05.16
 * Time: 4:31
 */
interface IDBModel {

    function __construct();

    function count();

    function countColumns();

    function get($id);

    function delete($id);

    function getList($settings);

}
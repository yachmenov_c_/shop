<?php

/**
 * Created by PhpStorm.
 * User: yaroslav
 * Date: 11.01.16
 * Time: 15:54
 */
class Urls extends UrlWorker {
    const notFoundUrl = '#';

    /**
     * @param array ...$arg_list
     *
     * 0) getRoute() - returns current route
     * 1) getRoute('Login') or getRoute('admin') - returns url by Router name from home directory or by key in RouterConfig
     * 2) getRoute('Login', 'register') or Urls::getRoute('admin', 'ManageProductsCategories')
     * 3, 4) getRoute('Products', 'viewDetailed', ['id' => 5]) - returns url built from Controller, Action and its params.
     *       getRoute('admin', 'ManageCategories', 'index') - returns completed url according to pattern from RouterConfig (only Router and Action)
     *       getRoute('admin', 'ManageProducts', 'edit', ['id' => 5]) - returns completed url according to pattern from RouterConfig with params
     *
     * @return string
     */
    public static function getRoute(...$arg_list) {
        $finished_url = null;

        switch (($arg_count = count($arg_list))) {
            case 0:
                $finished_url = self::getCurrentUrl();
                break;

            case 1:
                /* for controller or url by key in RouterConfigs */
                if (isset(RouterConfig::$readyRoutes[$arg_list[0]])) {
                    self::clearIfMainAction(RouterConfig::$readyRoutes[$arg_list[0]]['defaults']['action']);

                    $finished_url = self::getUnitedUrl(RouterConfig::$readyRoutes[$arg_list[0]]);
                    break;
                }
                $finished_url = class_exists($arg_list[0]) ? self::getUnitedUrl(['url' => $arg_list[0]]) : null;
                break;

            case 2:
                if (!isset(RouterConfig::$readyRoutes[$arg_list[0]])) {

                    $finished_url = class_exists($arg_list[0]) && method_exists($arg_list[0], $arg_list[1])
                        ? self::getUnitedUrl(['url' => implode('/', $arg_list)])
                        : null;
                    break;
                }

                $current_route = RouterConfig::$readyRoutes[$arg_list[0]];

                if (self::isReflectionNormal($arg_list[1], null, array()) !== false) {

                    $current_route['defaults']['controller'] = $arg_list[1];
                    $current_route['defaults']['action'] = '';

                    $finished_url = self::getUnitedUrl($current_route);
                }
                break;

            case 3:
            case 4:
                $routes_key = null;
                if ($arg_count === 4 || ( $arg_count === 3 && is_string($arg_list[2]) ) ) {
                    $routes_key = array_shift($arg_list);
                    array_push($arg_list, array());
                    if (!isset(RouterConfig::$readyRoutes[$routes_key])) break;
                }

                if (($method_args = self::isReflectionNormal($arg_list[0], $arg_list[1], $arg_list[2])) !== false) {
                    if ($routes_key === null) {
                        $conf = array('url' => $arg_list[0] . '/' . $arg_list[1] . '/', 'defaults' => array());
                    } else {
                        $conf = RouterConfig::$readyRoutes[$routes_key];
                        $conf['defaults']['controller'] = $arg_list[0];
                        $conf['defaults']['action'] = $arg_list[1];
                        if (substr($conf['url'], -1) !== '/') $conf['url'] .= '/';
                    }
                    foreach ($method_args as $m_a) {
                        $conf['url'] .= sprintf('%1$s/{%1$s}/', $m_a->name);
                        $conf['defaults'][$m_a->name] = $arg_list[2][$m_a->name];
                    }
                    $conf['url'] = substr($conf['url'], 0, -1);
                    $finished_url = self::getUnitedUrl($conf);
                }
                break;
        }

        return $finished_url === null ? self::notFoundUrl : strtolower($finished_url);
    }

    /**
     * @param string $class
     * @param string $method
     * @param array $args
     * @return bool|ReflectionParameter[]
     */
    protected static function isReflectionNormal($class, $method, array $args) {
        if (empty($method))
            $method = self::mainAction;
        if (class_exists($class) && method_exists($class, $method)) {
            $reflection = new ReflectionMethod($class, $method);
            if (count($method_args = $reflection->getParameters()) > count($args))
                return false;
            return $method_args;
        }
        return false;
    }

    public static function isHome() {
        return self::getControllerName() === RouterConfig::$readyRoutes['default']['defaults']['controller'];
    }

    public static function getCurrentUrl() {
        return parent::getCurrentUrl();
    }

    public static function getUrlPrefix() {
        return parent::getUrlPrefix();
    }

    public static function getControllerName() {
        return parent::getControllerName();
    }

    /**
     * @param string $action
     */
    protected static function clearIfMainAction(&$action) {
        if (self::isMainAction($action)) $action = '';
    }
}
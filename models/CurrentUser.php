<?php

/**
 * Created by PhpStorm.
 * User: yaroslav
 * Date: 13.01.16
 * Time: 23:52
 */
class CurrentUser {
    /* Time login in seconds*/
    protected static $loggedTime = 15 * 60;

    public static function isLogged() {
        if (self::getId() && isset($_SESSION['user']['login_time'])) {
            if (time() > $_SESSION['user']['login_time'] + self::$loggedTime) {
                self::logout();
                return false;
            }
            return true;
        }
        return false;
    }

    public static function isAdmin() {
        if (self::isLogged())
            if (!strcmp(self::getCategory(), 'admin'))
                return true;
        return false;
    }

    public static function logout() {
        session_unset();
        session_destroy();
    }

    public static function getId() {
        return !empty($_SESSION['user']['id']) ? $_SESSION['user']['id'] : false;
    }

    public static function getCategory() {
        return !empty($_SESSION['user']['category']) ? $_SESSION['user']['category'] : false;
    }

    public static function getEmail() {
        return !empty($_SESSION['user']['email']) ? $_SESSION['user']['email'] : false;
    }

    public static function getName() {
        return !empty($_SESSION['user']['name']) ? $_SESSION['user']['name'] : false;
    }


    public static function startSession($user_id, $password) {
        if (!empty($user_id)) {
            $users = new DBUsers();
            if ($id = $users->getId($user_id) && $users->isRightPassword($user_id, $password)) {
                $user = $users->get($user_id);
                $_SESSION['user'] = array(
                    'id'                  => $user['id'],
                    'name'                => $user['name'],
                    'email'               => $user['email'],
                    'category'            => $users->getCategoryName($user['id']),
                    'admin_panel_visited' => false,
                    'login_time'          => time()
                );
                return self::isLogged();
            }
        }
        return false;
    }

}
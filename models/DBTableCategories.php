<?php

/**
 * Created by PhpStorm.
 * User: yaroslav
 * Date: 18.01.16
 * Time: 2:54
 */
abstract class DBTableCategories extends DBView {

    protected $tableOfCategories = null;

    public function getList($settings = array('order_by'    => '', 'order' => 'ASC',
                                              'search_type' => 'this', 'value' => '')) {
        if (!isset($settings['order_by'])) $settings['order_by'] = '';
        if (!isset($settings['order'])) $settings['order'] = 'ASC';
        if (!isset($settings['search_type'])) $settings['search_type'] = 'this';
        switch ($settings['search_type']) {
            case 'this':
                $sql = "SELECT * FROM {$this->table}";
                if (!empty($settings['order_by']))
                    $sql .= " ORDER BY {$settings['order_by']}";
                $result = $this->pdo->prepare($sql);
                break;
            case 'category':
                if (!isset($settings['value'])) $settings['value'] = '';
                switch (gettype($settings['value'])) {
                    case 'string':
                        $sql = "SELECT `table`.*
                        FROM {$this->table} AS `table`, {$this->tableOfCategories} AS `tableOfCategories`
                        WHERE  `table`.`category_id` = `tableOfCategories`.`id` AND `tableOfCategories`.`name` = ?";
                        $param_type = PDO::PARAM_STR;
                        break;
                    case 'integer':
                        $sql = "SELECT `p`.*
                        FROM {$this->table} AS `p`
                        WHERE  `p`.`category_id` = ?";
                        $param_type = PDO::PARAM_INT;
                        break;
                    default:
                        return false;
                }
                if (!empty($settings['order_by']))
                    $sql .= " ORDER BY {$settings['order_by']} {$settings['order']}";
                $result = $this->pdo->prepare($sql);
                $result->bindValue(1, $settings['value'], $param_type);
                break;
        }
        $result->execute();
        return $result->fetchAll(PDO::FETCH_ASSOC);
    }

    protected function getId($params) {
        if (!isset($params['table']))
            $params['table'] = $this->table;
        switch (gettype($params['value'])) {
            case 'integer':
                $sql = "SELECT `id` FROM {$params['table']} WHERE `id` = {$params['value']};";
                $res = $this->pdo->prepare($sql);
                $res->execute();
                break;
            case 'string':
                $sql = "SELECT `id` FROM {$params['table']} WHERE `name` = :value_name;";
                $res = $this->pdo->prepare($sql);
                $res->bindValue(':value_name', $params['value']);
                $res->execute();
                break;
        }
        return isset($res) ? $res->fetch(PDO::FETCH_NUM)[0] : false;
    }

}
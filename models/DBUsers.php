<?php

/**
 * Created by PhpStorm.
 * User: yaroslav
 * Date: 11.01.16
 * Time: 18:20
 */
class DBUsers extends DBTableCategories {
    protected $table = 'users';
    protected $tableOfCategories = 'users_categories';

    public function getCategoryId($name) {
        return parent::getId(['table' => $this->tableOfCategories, 'value' => $name]);
    }

    public function getId($user) {
        return parent::getId(['value' => $user]);
    }

    public function getCategoryName($id) {
        if (!is_int($id))
            $id = (int)$id;
        $sql = "SELECT `cat_u`.`name` FROM {$this->tableOfCategories} AS `cat_u`, {$this->table} AS `u`
                WHERE `u`.`id` = {$id} AND `u`.`category_id` = `cat_u`.`id`";
        $res = $this->pdo->prepare($sql);
        $res->execute();
        return $res->fetch(PDO::FETCH_NUM)[0];
    }

    public function isRightPassword($id, $password) {
        if (!is_int($id))
            $id = (int)$id;
        $sql = "SELECT `password` FROM {$this->table} WHERE `id` = {$id}";
        $res = $this->pdo->prepare($sql);
        $res->execute();
        $user_password = $res->fetch(PDO::FETCH_NUM)[0];
        if (!strcmp(md5($password), $user_password))
            return true;
        return false;
    }

    public function add($data) {
        if (!empty($data) && count($data) == $this->countColumns() - 1) {
            $data = array_values($data);
            $data_final = array();
            list($data_final['name'], $data_final['password'],
                $data_final['email'], $data_final['category_id']) = $data;
            $sql = "INSERT INTO $this->table(`name`, `password`, `email`, `category_id`)
                    VALUES(:name, :password, :email, :category_id)";
            $res = $this->pdo->prepare($sql);
            $res->bindValue(':name', $data_final['name']);
            $res->bindValue(':password', $data_final['password']);
            $res->bindValue(':email', $data_final['email']);
            $res->bindValue(':category_id', $data_final['category_id']);
            return (bool)$res->execute();
        }
        return false;
    }

    public function updateName($id, $name) {
        $this->updateField('name', $id, $name);
    }

    public function updatePassword($id, $password) {
        $this->updateField('password', $id, md5($password));
    }

    public function updateEmail($id, $email) {
        $this->updateField('email', $id, $email);
    }

    protected function updateField($field_name, $id, $value){
        if (is_numeric($id)) {
            $sql = "UPDATE {$this->table} SET {$field_name} = ? WHERE `id` = ?";
            $res = $this->pdo->prepare($sql);
            $res->bindValue(1, $value, PDO::PARAM_STR);
            $res->bindValue(2, $id, PDO::PARAM_INT);
            return $res->execute();
        }
        return false;
    }

}
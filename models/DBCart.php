<?php

/**
 * Created by PhpStorm.
 * User: yaroslav
 * Date: 30.05.16
 * Time: 4:22
 */
class DBCart extends DBView{
    protected $table = 'basket_items';
    /**
     * @var DBProducts $products
     */
    protected $products;

    public function __construct() {
        parent::__construct();
        $this->products = new DBProducts;
    }

    /**
     * @param array $settings
     * 1) default $this->getList()
     * 2) $this->getList( array('search_type' => 'user', 'user_id' => 2) ); - getting all notes about specific user
     * @return bool|array
     */
    public function getList($settings = array('search_type' => 'this')) {
        $sql = "SELECT * FROM {$this->table} ";
        if(!isset($settings['search_type']))
            $settings = array('search_type' => 'this');

        switch($settings['search_type']){
            case 'user':
                if(!isset($settings['user_id']))
                    $settings['user_id'] = 0;

                $sql .= "WHERE `user_id` = ?";
                $res = $this->pdo->prepare($sql);
                $res->bindValue(1, $settings['user_id'], PDO::PARAM_INT);
                break;

            default:
                $res = $this->pdo->prepare($sql);
                break;
        }

        return $res->execute() ? $res->fetchALL(PDO::FETCH_ASSOC) : false;
    }

    public function add($data) {
        if (!empty($data) && count($data) == $this->countColumns() - 3) {
            $data = array_values($data);
            $data_final = array();
            list($data_final['user_id'], $data_final['product_id']) = $data;

            $res = $this->pdo->prepare("SELECT COUNT(*) FROM {$this->table} WHERE :user_id = `user_id` AND :product_id = `product_id`");
            $this->bindIds($res, $data_final['user_id'], $data_final['product_id']);
            $counter = $res->execute() ? $res->fetch(PDO::FETCH_NUM)[0] : 0;
            if($counter > 1) return false;

            if($counter == 1){
                $sql = "UPDATE {$this->table} SET `product_count` = `product_count` + 1, `date_time` = CURRENT_TIMESTAMP WHERE :user_id = `user_id` AND :product_id = `product_id`";
                $res = $this->pdo->prepare($sql);
            } else {
                $res = $this->pdo->prepare("INSERT INTO {$this->table}(`user_id`, `product_id`) VALUES(:user_id, :product_id)");
            }
            $this->bindIds($res, $data_final['user_id'], $data_final['product_id']);

            return $res->execute() ? $this->products->decStock($data_final['product_id']) : false;
        }
        return false;
    }

    /**
     * @param int|void - user_id can be placed here
     * @return bool
     */
    public function count() {
        if (func_num_args() >= 1)
            $user_id = func_get_arg(0);
        if (empty($user_id))
            return parent::count();
        else {
            $res = $this->pdo->prepare("SELECT COUNT(*) FROM {$this->table} WHERE `user_id` = ?");
            $res->bindValue(1, $user_id, PDO::PARAM_INT);
            return $res->execute() ? $res->fetch(PDO::FETCH_NUM)[0] : false;
        }
    }

    public function delete($id) {
        $current_item = $this->get($id);
        return $this->products->shiftStock($current_item['product_id'], $current_item['product_count']) ? parent::delete($id) : false;
    }

    protected function bindIds(PDOStatement $res, $user_id, $product_id){
        $res->bindValue(':user_id', $user_id);
        $res->bindValue(':product_id', $product_id);
    }
}
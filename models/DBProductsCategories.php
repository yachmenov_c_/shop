<?php

/**
 * Created by PhpStorm.
 * User: yaroslav
 * Date: 18.01.16
 * Time: 2:48
 */
class DBProductsCategories extends DBTableCategories {
    protected $table = 'products_categories';

    public function update(array $data = array('id' => null, 'name' => '')) {
        if (!empty($data)) {
            $sql = "UPDATE {$this->table} SET `name` = ?
                    WHERE `id` = ?";
            $res = $this->pdo->prepare($sql);
            $res->bindValue(1, $data['name']);
            $res->bindValue(2, $data['id']);
            return (bool)$res->execute();
        }
        return false;
    }
}
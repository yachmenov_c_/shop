/**
 * Created by yaroslav on 30.01.16.
 */
$.fn.hasScrollBar = function () {
    return this.get(0).scrollHeight > this.height();
};

$.fn.drawItemsOfCart = function(){
    $.ajax({
            method: "POST",
            url: "/Cart/countMenu",
            dataType: "json"
        })
        .done(function (data) {
            if(data !== -1){
                $('.navbar-logo').parent().append('<span class="badge">' + data + '</span>');
            }
        });
};

$(function () {
    $().drawItemsOfCart();
    if ($('body').hasScrollBar())
        $('footer').css({position: 'relative'});
    window.onresize = function () {
        var footer = $('footer');
        if ($('body').hasScrollBar()) {
            if (footer.css('position') === 'absolute')
                footer.css({position: 'relative'});
        }
        else {
            if (footer.css('position') === 'relative')
                footer.css({position: 'absolute'});
        }
    };
});

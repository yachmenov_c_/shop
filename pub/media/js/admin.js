/**
 * Created by yaroslav on 14.01.16.
 **/
$(document).ready(function () {
    $.ajax({
        method: "POST",
        url: yyo_admin_url + '/AdminAjax/panelVisited',
        data: {visited: true},
        dataType: "json"
    });
});
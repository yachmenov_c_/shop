/**
 * Created by yaroslav on 16.01.16.
 */

$(document).ready(function () {
    $.ajax({
            method: "POST",
            url: "/Categories/listMenu",
            dataType: "json"
        })
        .done(function (data) {
            for (var i = 0; i < data.length; i++) {
                $('#store-menu').append('<li>' +
                    '<a href="' + data[i].url + '">' + data[i].name + '</a>' +
                    '</li>');
            }
        });
});
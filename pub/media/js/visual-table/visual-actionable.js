/**
 * Created by yaroslav on 07.02.16.
 */
$(function () {
    var visual_action_modal = $('#visual-action-modal');
    var active_action = {
        href: '',
        data: ''
    };
    visual_action_modal.modal({show: false});
    $("a.visual-action-delete").click(function () {
        active_action.href = this.href;
        active_action.data = $(this).parent().parent().children('td');
        visual_action_modal.modal('show');
        return false;
    });
    visual_action_modal.on('show.bs.modal', function () {
        var modal = $(this);
        modal.find('.modal-body').append('<div class="table-responsive"><table class="table"><tr></tr></table></div>');
        var tr = modal.find('tr');
        for (var i = 0; i < active_action.data.length; i++)
            if (!$(active_action.data[i]).children('a').hasClass('visual-action-normal'))
                tr.append($(active_action.data[i]).clone());
    });
    visual_action_modal.on('hidden.bs.modal', function () {
        $(this).find('.modal-body').get(0).innerHTML = '';
    });
    $("#visual-action-modal-delete").click(function () {
        visual_action_modal.modal('hide');
        $(location).attr('href', active_action.href);
    });
});
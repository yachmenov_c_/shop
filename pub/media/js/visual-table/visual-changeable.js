/**
 * Created by yaroslav on 5.02.16.
 **/
$(document).ready(function () {
    var before_active = null;
    $.fn.extend({
        visual_textarea_append: function () {
            if (!$(this).children().is('textarea')) {
                var content_text = $(this).text();
                var new_textarea = $('<textarea style="width:100%;display:none;">' + content_text + '</textarea>').hide();
                $(this).empty();
                $(this).append(new_textarea);
                new_textarea.fadeIn(500);
            }
        }
    });
    $.fn.extend({
        visual_textareas_clear: function () {
            var textareas = $('.visual-changeable tbody  tr > td > textarea');
            for (var i = 0; i < textareas.length; i++) {
                $(textareas[i]).fadeOut(500, function(){
                    var content_text = $(this).val();
                    var td = $(this).parent();
                    td.empty();
                    td.text(content_text);
                });
            }
        }
    });
    $.fn.extend({
        visual_table_ajax_save: function (id, key, data, done_func) {
            $.ajax({
                    method: "POST",
                    url: $().visual_changeable_url(),
                    data: {
                        id: id,
                        key: key,
                        data_field: data
                    },
                    dataType: "json"
                })
                .done(done_func);
        }
    });
    $.fn.extend({
        button_changeable: function (str_param) {
            var button = $("#visual-changeable-button");
            switch (str_param) {
                case 'show':
                    button.toggle("slow");
                    break;
                case 'hide':
                    button.toggle("slow");
                    break;
                case 'load':
                    button.button('loading');
                    break;
                case 'unload':
                    button.button('reset');
                    break;
                case 'event_start':
                    button.on('click', function () {
                        $().button_changeable('hide');
                        $().visual_table_ajax_save($().get_row_id(), $().get_col_key(), $().get_data_field(),
                            function (data_status) {
                                console.log(data_status);
                                before_active.visual_textareas_clear();
                                before_active = null;
                            });
                    });
                    break;
            }
        }
    });
    $.fn.extend({
        get_data_field: function () {
            return before_active.children('textarea').val();
        }
    });
    $.fn.extend({
        get_col_key: function () {
            return before_active.attr('visual-changeable-data');
        }
    });
    $.fn.extend({
        get_row_id: function () {
            return before_active.parent().children("td[visual-requires-data = 'id']").text();
        }
    });
    $().button_changeable('event_start');
    $(".visual-changeable tbody  tr > td[visual-changeable-data]").on('dblclick', function () {
        var now_active = $(this);
        if (before_active === null) {
            $().button_changeable('show');
            now_active.visual_textarea_append();
        } else {
            if (before_active.get(0) !== now_active.get(0)) {
                if (before_active.children('textarea').val() == before_active.text()) {
                    before_active.visual_textareas_clear();
                    now_active.visual_textarea_append();
                } else {
                    $().button_changeable('load');
                    $().visual_table_ajax_save($().get_row_id(), $().get_col_key(), $().get_data_field(),
                        function (data_status) {
                            console.log(data_status);
                            $().button_changeable('unload');
                            before_active.visual_textareas_clear();
                            now_active.visual_textarea_append();
                        });
                }
            }
        }
        before_active = now_active;
    });
});

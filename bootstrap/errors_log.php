<?php

function run_error_log() {
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
    check_config_file();
    $log_file = DIR_LOG . 'system.log';
    if (!DEV_DISPLAY_ERRORS) {
        if (!file_exists($log_file)) {
            if (!mkdir(DIR_LOG, 0755, true)) {
                trigger_error("www-data or apache have not permission for creating dirs
                 and files (location: " . __ROOT__ . ")", E_USER_ERROR);
                die;
            }
            touch($log_file);
            if (0755 !== (fileperms(DIR_LOG) & 0777))
                @exec('chmod -R 0755 ' . escapeshellarg(DIR_LOG));
        }
        set_error_handler(function ($num, $str, $file, $line, $context = null) {
            throw new ErrorException($str, 0, $num, $file, $line);
        });

        set_exception_handler(function ($e) use ($log_file) {
            $message = "Type: " . get_class($e) . "; Message: {$e->getMessage()}; File: {$e->getFile()}; Line: {$e->getLine()};";
            file_put_contents($log_file, date('l jS \of F h:i:s A') . PHP_EOL . $message . PHP_EOL, FILE_APPEND);
            ob_clean();
            print((new Page500)->{UrlWorker::mainAction}());
        });

        register_shutdown_function(function () use ($log_file) {
            $error = error_get_last();
            if ($error['type'] == E_ERROR) {
                $message = "Message: {$error['message']}; File: {$error['file']}; Line: {$error['line']};";
                file_put_contents($log_file, date('l jS \of F h:i:s A') . PHP_EOL . $message . PHP_EOL, FILE_APPEND);
            }
        });
        ini_set('display_errors', 0);
    }
}

function check_config_file() {
    if (!file_exists(PATH_CONFIG)) {
        $message = 'File for database access isn\'t exist! Path: ' . PATH_CONFIG;
        trigger_error($message, E_USER_ERROR);
        die("<div style='text-align: center'>$message</div>");
    } else {
        if (!defined('DEV_DISPLAY_ERRORS'))
            define('DEV_DISPLAY_ERRORS', 0);
        if (!defined('DEV_RELOAD_CLASSES'))
            define('DEV_RELOAD_CLASSES', 0);
    }
}
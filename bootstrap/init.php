<?php
define('__ROOT__', dirname(__DIR__) . DIRECTORY_SEPARATOR);
define('URL_MEDIA', DIRECTORY_SEPARATOR .  'media' . DIRECTORY_SEPARATOR);
define('URL_IMG', URL_MEDIA . 'images' . DIRECTORY_SEPARATOR);
define('URL_CSS', URL_MEDIA . 'css' . DIRECTORY_SEPARATOR);
define('URL_JS', URL_MEDIA . 'js' . DIRECTORY_SEPARATOR);
define('DIR_LOG', __ROOT__ . 'logs' . DIRECTORY_SEPARATOR);
define('DIR_BOOTSTRAP', __DIR__ . DIRECTORY_SEPARATOR);
define('DIR_APP', __ROOT__ . 'app' . DIRECTORY_SEPARATOR);
define('PATH_CONFIG', __ROOT__ . 'config.php');
define('LANG_DIR', DIR_APP . 'languages' . DIRECTORY_SEPARATOR);
/*define('LANG', 'uk_UA');*/

require(PATH_CONFIG);
require(DIR_BOOTSTRAP . 'errors_log.php');
run_error_log();
session_start();

require(DIR_APP . 'interfaces/ISingleton.php');
require(DIR_APP . 'interfaces/INoSingleton.php');
require(DIR_APP . 'workers/SingletonWorker.php');
require(DIR_APP . 'Connect.php');
require(DIR_APP . 'workers/SearchWorker.php');
require(DIR_APP . 'workers/DBWorker.php');
require(DIR_APP . 'Install.php');

Install::instance()->run();

spl_autoload_register(function ($class) {
    static $db_classes = null;
    if ($db_classes === null) {
        $new_install = !DBWorker::getOption('classes_configured', false);
        if ($new_install || DEV_RELOAD_CLASSES) {
            $class_map = DIR_BOOTSTRAP . 'json/class_dirs.json';
            $config_classes = json_decode(file_get_contents($class_map), true);
            $db_classes = array();
            foreach ($config_classes as $dir_name => $options) {
                $file_paths = $options['recursive'] ? SearchWorker::rGlob(__ROOT__ . "{$dir_name}/*.php")
                    : SearchWorker::rGlob(__ROOT__ . "{$dir_name}/*.php", null, false);
                foreach ($file_paths as $path) {
                    $parts = explode('.', $path);
                    end($parts);
                    $temp_class = explode(DIRECTORY_SEPARATOR, prev($parts));
                    $temp_class = end($temp_class);
                    $db_classes[$temp_class] = $path;
                }
            }
            if (@file_put_contents(DIR_BOOTSTRAP . 'json/class_map.json', json_encode($db_classes), LOCK_EX) === false) {
                throw new Exception("www-data or apache have not permission for creating dirs and files (location: " . DIR_BOOTSTRAP . ")");
            }
            if ($new_install)
                DBWorker::setOption('classes_configured', true, false);
        } else {
            if (($contents = @file_get_contents(DIR_BOOTSTRAP . 'json/class_map.json')) === false)
                throw new Exception(DIR_BOOTSTRAP . "json/class_map.json isn't exist, make classes refresh/reload, please!");
            $db_classes = json_decode($contents, true);
        }
        SearchWorker::$dbClasses = $db_classes;
    }
    if (isset($db_classes[$class]))
        require($db_classes[$class]);
});

require(LANG_DIR . 'l18n.php');
load_default_textdomain();
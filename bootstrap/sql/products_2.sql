CREATE TABLE IF NOT EXISTS `products_categories` (
  `id`    INT(11)      NOT NULL AUTO_INCREMENT,
  `name`  NVARCHAR(50) NOT NULL,
  `image` NVARCHAR(255)         DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY (`name`)
);

--
-- Dumping data for table `users_categories`
--

INSERT IGNORE INTO `products_categories` (`id`, `name`, `image`) VALUES
  (1, 'Laptops', 'laptops.png'),
  (2, 'Computer hardware', 'hard.png'),
  (3, 'Coffee products', 'coffee.png');


CREATE TABLE IF NOT EXISTS `products` (
  `id`          INT(11)       NOT NULL AUTO_INCREMENT,
  `description` TEXT                   DEFAULT NULL,
  `name`        NVARCHAR(255) NOT NULL,
  `price`       DECIMAL(8, 2) NOT NULL,
  `image`       NVARCHAR(255)          DEFAULT NULL,
  `in_stock`    INT(11)                DEFAULT NULL,
  `category_id` INT(11)       NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY (`name`),
  FOREIGN KEY (`category_id`) REFERENCES `products_categories` (`id`)
    ON DELETE CASCADE
);

--
-- Dumping data for table `products`
--

INSERT IGNORE INTO `products` (`name`, `description`, `price`, `image`, `in_stock`, `category_id`) VALUES
  ('HP 250 G4 Notebook PC',
   'Windows 7 Professional 64 (available through downgrade rights from Windows 10 Pro 64) / Intel® Core™ i3-5005U with Intel HD Graphics 5500 (2 GHz, 3 MB cache, 2 cores) / 4 GB DDR3L-1600 SDRAM (1 x 4 GB)',
   369.00, 'hp_250_g4.png', 5, 1),
  ('HP OMEN Notebook - 15-5210nr',
   'Windows 10 Home 64 / Intel® Core™ i7 processor / 8 GB DDR3L SDRAM (onboard) / 15.6" diagonal FHD IPS Radiance Infinity LED-backlit touch screen (1920 x 1080)',
   1599.99, 'hp_omen_15.png', 2, 1),
  ('ThinkPad E465 Laptop',
   'Latest AMD cutting-edge processors / Up to Windows 10 Pro / Optional discrete graphics / Optional fingerprint reader / Brilliant 14" up-to-FHD display / Hi-fidelity superior sound with enhanced audio',
   455.05, 'lenovo_think_e465.png', 3, 1);

INSERT IGNORE INTO `products` (`name`, `description`, `price`, `image`, `in_stock`, `category_id`) VALUES
  ('Intel Core i7-6700K',
   '8M Skylake Quad-Core 4.0 GHz Desktop Processor Intel® HD Graphics 530 / LGA 1151 / Unlocked Processor / DDR4 & DDR3L Support / Display Resolution up to 4096x2304 / Intel Turbo Boost Technology / Compatible with Intel 100 Series Chipset Motherboards',
   413.99, 'intel_core_i7_6700K.png', 10, 2),
  ('Intel Core i7-5960X',
   'Haswell-E 8-Core 3.0 GHz Desktop Processor / 22nm Haswell-E 140W / 20MB L3 Cache / 8 x 256KB L2 Cache',
   1049.99, 'intel_core_i7_5960X.png', 15, 2),
  ('MSI Gaming X99A Motherboard',
   'Intel X99 / Core i7 (LGA2011-v3) / DDR4 3400*(*OC)/ 3333*/ 3110*/ 3000*/ 2800*/ 2750*/ 2666*/ 2600*/ 2400*/ 2200*/ 2133',
   549.99, 'msi_gaming_motherboard_X99.png', 1, 2);

INSERT IGNORE INTO `products` (`name`, `description`, `price`, `image`, `in_stock`, `category_id`) VALUES
  ('Blaser Cafe Sera Caffeine-free Coffee',
   'Sera is decaffeinated with pure water vapour by a partner in Switzerland. We determine the composition and quality of the coffee used. This means you can still enjoy Blasercafé quality when drinking decaffeinated coffee. / Degree of roast / Medium',
   85.00, 'sera_250g_medium.png', 50, 3);

--
-- BASKET LOGIC
--

CREATE TABLE IF NOT EXISTS `basket_items` (
  `id`            INT(11) NOT NULL AUTO_INCREMENT,
  `user_id`       INT(11) NOT NULL,
  `product_id`    INT(11) NOT NULL,
  `product_count` INT(11) DEFAULT 1,
  `date_time`     DATETIME         DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
    ON DELETE CASCADE,
  FOREIGN KEY (`product_id`) REFERENCES `products` (`id`)
    ON DELETE CASCADE
);
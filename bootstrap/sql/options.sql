CREATE PROCEDURE `update_option`(IN f_name VARCHAR(255), IN f_data TEXT)
  BEGIN
    DECLARE is_new INT(11);

    SET is_new = (SELECT COUNT(`name`) FROM `options` WHERE `name` = f_name);

    IF is_new = 0 THEN
      INSERT INTO `options` (`name`, `data`) VALUES (f_name, f_data);
    ELSE
      UPDATE `options`
      SET `data` = f_data, `date_time` = CURRENT_TIMESTAMP
      WHERE `name` = f_name;
    END IF;

  END;

CREATE FUNCTION `get_option`(f_name VARCHAR(255))
  RETURNS TEXT
  RETURN (SELECT `data`
          FROM `options`
          WHERE `name` = f_name);

CREATE TABLE `options` (
  `id`        INT(11)       AUTO_INCREMENT,
  `name`      NVARCHAR(255) NOT NULL,
  `data`      TEXT     DEFAULT NULL,
  `date_time` DATETIME DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY (`name`)
);

INSERT IGNORE INTO `options` (`id`, `name`, `data`) VALUES
  (1, 'classes_configured', FALSE),
  (2, 'installed_files', ''),
  (3, 'available_languages', 'en_US-English,uk_UA-Ukrainian'),
  (4, 'available_countries', 'UA-Ukraine,AT-Austria,CA-Canada,CZ-Czech Republic,DE-Germany,GR-Greece,IT-Italy,BY-Belarus,GB-United Kingdom');

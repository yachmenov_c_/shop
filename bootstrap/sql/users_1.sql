CREATE TABLE IF NOT EXISTS `users_categories` (
  `id`   INT(11)      NOT NULL AUTO_INCREMENT,
  `name` NVARCHAR(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY (`name`)
);

--
-- Dumping data for table `users_categories`
--

INSERT IGNORE INTO `users_categories` (`id`, `name`) VALUES
  (1, 'user'),
  (2, 'editor'),
  (3, 'admin');

CREATE TABLE IF NOT EXISTS `users` (
  `id`          INT(11)      NOT NULL AUTO_INCREMENT,
  `name`        NVARCHAR(50) NOT NULL,
  `password`    NVARCHAR(50) NOT NULL,
  `email`       NVARCHAR(50) NULL,
  `category_id` INT(11)      NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY (`email`),
  UNIQUE KEY (`name`),
  FOREIGN KEY (`category_id`) REFERENCES `users_categories` (`id`)
    ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS `users_meta`(
  `id`          INT(11)      NOT NULL AUTO_INCREMENT,
  `user_id`     INT(11)      NOT NULL,
  `name`        NVARCHAR(50) NOT NULL,
  `data`        TEXT         NULL,
  `date_time`   DATETIME DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
    ON DELETE CASCADE
);

CREATE PROCEDURE `update_user_meta`(IN f_user_id INT(11), IN f_name VARCHAR(255), IN f_data TEXT)
  BEGIN
    DECLARE is_new INT(11);

    SET is_new = (SELECT COUNT(`name`) FROM `users_meta` WHERE `name` = f_name AND `user_id` = f_user_id);

    IF is_new = 0 THEN
      INSERT INTO `users_meta` (`user_id`, `name`, `data`) VALUES (f_user_id, f_name, f_data);
    ELSE
      UPDATE `users_meta`
      SET `data` = f_data, `date_time` = CURRENT_TIMESTAMP
      WHERE `name` = f_name AND `user_id` = f_user_id;
    END IF;

  END;

CREATE FUNCTION `get_user_meta`(`f_user_id` INT(11),`f_name` VARCHAR(255))
  RETURNS TEXT
  RETURN (SELECT `data`
          FROM `users_meta`
          WHERE `name` = f_name AND `user_id` = f_user_id);
<?php

class Home extends Controller {
    public function index() {
        return $this->view(array(
            'title' => __('Home Page')
        ));
    }

}
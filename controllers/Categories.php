<?php

/**
 * Created by PhpStorm.
 * User: yaroslav
 * Date: 29.01.16
 * Time: 17:54
 */
class Categories extends Controller {
    protected $categoriesStock;

    public function __construct() {
        $this->categoriesStock = new DBProductsCategories();
    }

    public function index() {
        $list = $this->categoriesStock->getList(array('order_by' => 'name'));
        $productsStock = new DBProducts();
        foreach ($list as &$l)
            $l['count'] = $productsStock->count($l['id']);
        return $this->view(array(
            'data'  => array('list' => $list, 'view' => 'all'),
            'title' => __('All categories')
        ));
    }

    public function listMenu() {
        $products_cat = $this->categoriesStock->getList(['order_by' => 'name']);
        $data = array();
        foreach ($products_cat as $cat) {
            $data[] = array(
                'name' => $cat['name'],
                'url'  => Urls::getRoute(get_class(), 'viewDetailed', ['id' => $cat['id']])
            );
        }
        return $this->json($data);
    }

    public function viewDetailed($id) {
        if (!empty($id)) {
            $productsStock = new DBProducts();
            $data = $productsStock->getList(array(
                'value'       => (int)$id,
                'order_by'    => 'price',
                'search_type' => 'category'
            ));
            if (empty($data)){
                $data = array();
            }
            foreach($data as &$item){
                $item['add_to_cart'] = Urls::getRoute('Cart', 'add', ['productId' => $item['id']]);
            }
            return $this->view(array(
                'title' => $this->categoriesStock->get($id)['name'],
                'data'  => $data
            ));
        }
        return $this->page404();
    }
}
<?php

/**
 * Created by PhpStorm.
 * User: yaroslav
 * Date: 30.05.16
 * Time: 4:02
 */
class Cart extends Controller{

    protected $productsStock;
    protected $cart;

    public function __construct(){
        if(!CurrentUser::isLogged() && self::getActionName() !== 'countMenu'){
            $this->redirect(Urls::getRoute('Login'));
            return;
        }
        $this->productsStock = new DBProducts;
        $this->cart = new DBCart;
        Page::$title = __('My Cart');
    }

    public function index(){
        $data = $this->cart->getList( array('search_type' => 'user', 'user_id' => CurrentUser::getId()) );

        foreach($data as &$item){
            $item['product'] = $this->productsStock->get($item['product_id']);
            $item['date_time'] = strtotime($item['date_time']);
            unset($item['product_id']);
        }

        usort($data, function($a, $b){
            return $a['date_time'] < $b['date_time'];
        });

        $data['is_shipping_fine'] = $this->isShippingFine();

        return $this->view(array(
            'data' => $data
        ));
    }

    public function add($productId){
        if(is_numeric($productId)){
            $product_data = $this->productsStock->get($productId);
            if($product_data && $product_data['in_stock'] > 0){
                $this->cart->add(array('user_id' => CurrentUser::getId(), 'product_id' => $product_data['id']));
                $this->redirect(Urls::getRoute(__CLASS__));
            }
        }
        return $this->page404();
    }

    public function delete($id){
        if(is_numeric($id)){
            $this->cart->delete($id);
            return $this->index();
        }
        return $this->page404();
    }

    public function countMenu() {
        if (CurrentUser::isLogged()) {
            $data = $this->cart->count(CurrentUser::getId());
            return $this->json($data);
        } else {
            return $this->json(-1);
        }
    }

    protected function isShippingFine(){
        $user_id = CurrentUser::getId();
        $user = array();

        $user['shipping_name'] = DBWorker::getUserMeta($user_id, 'shipping_name', false);
        $user['shipping_street'] = DBWorker::getUserMeta($user_id, 'shipping_street', false);
        $user['shipping_city'] = DBWorker::getUserMeta($user_id, 'shipping_city', false);
        $user['shipping_state'] = DBWorker::getUserMeta($user_id, 'shipping_state', false);
        $user['shipping_country'] = DBWorker::getUserMeta($user_id, 'shipping_country', false);
        $user['shipping_zip'] = DBWorker::getUserMeta($user_id, 'shipping_zip', false);
        $user['shipping_phone'] = DBWorker::getUserMeta($user_id, 'shipping_phone', false);

        $user = array_filter($user, function($element){
            return (empty($element) || ctype_space($element)) ? true : false;
        });

        return count($user) > 0 ? false : true;
    }

}
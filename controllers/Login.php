<?php

/**
 * Created by PhpStorm.
 * User: yaroslav
 * Date: 13.01.16
 * Time: 18:39
 */
class Login extends Controller {

    public function __construct() {
        if (CurrentUser::isLogged() && strcmp(self::getActionName(), 'out'))
            $this->redirect(Urls::getRoute('default'));
        else Page::$title = __('Login');
    }

    public function index() {
        $this->checkIsAdminInDB();
        $data['good'] = $this->checkForPassName();
        if ($data['good'] !== false) {
            $users = new DBUsers();
            list($data['good']['username'], $data['good']['password']) = false;
            if ($id = (int)$users->getId($_POST['username'])) {
                $data['good']['username'] = true;
                if (CurrentUser::startSession($id, $_POST['password']))
                    $this->redirect(Urls::getRoute('default'));
            }
        }
        return $this->view(array(
            'data' => $data
        ));
    }

    public function register() {
        $this->checkIsAdminInDB();
        $data['good'] = $this->checkForPassName();
        if ($data['good'] !== false) {
            $users = new DBUsers();
            $res = $users->add(array(
                $_POST['username'], md5($_POST['password']),
                !empty($_POST['email']) ? $_POST['email'] : null, $users->getCategoryId('user')
            ));
            if ($res) $this->redirect(Urls::getRoute(get_class($this)));
        }
        return $this->view(array(
            'title' => __('Register page'),
            'data'  => $data
        ));
    }

    public function out() {
        CurrentUser::logout();
        self::redirect(Urls::getRoute(get_class($this)));
    }

    public function createAdmin() {
        /** @var $connection Connect */
        $data = null;
        if (isset($_POST['password-sql']) && ($data['good'] = $this->checkForPassName()) !== false) {
            $connection = Connect::instance();
            $data['good']['password-sql'] = false;
            if (md5($_POST['password-sql']) === $connection->getPassword()) {
                $data['good']['password-sql'] = true;
                $users = new DBUsers();
                $res = $users->add(array(
                    $_POST['username'], md5($_POST['password']),
                    null, $users->getCategoryId('admin')
                ));
                if ($res) $this->redirect(Urls::getRoute(get_class($this)));
            }
        }
        return $this->view(array(
            'title' => __('Admin creation'),
            'data'  => $data
        ));
    }

    private function checkIsAdminInDB() {
        $users = new DBUsers();
        if (!count($users->getList(['search_type' => 'category', 'value' => 'admin'])))
            $this->redirect(Urls::getRoute(get_class($this), 'createAdmin'));
    }

    private function checkForPassName() {
        if (isset($_POST['username']) && isset($_POST['password'])) {
            $data = ['username' => false, 'password' => false];
            if (strlen($_POST['username']) >= 6 && strlen($_POST['username']) <= 50)
                $data['username'] = true;
            if (strlen($_POST['password']) >= 6 && strlen($_POST['password']) <= 20)
                $data['password'] = true;
            if (array_search(false, $data) === false)
                return $data;
        }
        return false;
    }

}
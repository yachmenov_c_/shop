<?php

/**
 * Created by PhpStorm.
 * User: yaroslav
 * Date: 17.01.16
 * Time: 0:29
 */
class Products extends Controller {
    protected $productsStock;

    public function __construct() {
        $this->productsStock = new DBProducts;
    }

    public function index() {
        $list = $this->productsStock->getList(array('order_by' => 'price'));
        foreach($list as &$item){
            $item['add_to_cart'] = Urls::getRoute('Cart', 'add', ['productId' => $item['id']]);
        }
        return $this->view(array(
            'title' => __('All products'),
            'data'  =>  $list,
            'name'  => 'stock/Categories.phtml'
        ));
    }

    public function viewDetailed($id) {
        if (!empty($id)) {
            $product = $this->productsStock->get($id);
            $product['add_to_cart'] = Urls::getRoute('Cart', 'add', ['productId' => $product['id']]);
            if (!empty($product))
                return $this->view(array(
                    'title' => $product['name'],
                    'data'  => $product
                ));
        }
        return $this->page404();
    }
}
<?php

class Page404 extends Controller {
    public function index() {
        return $this->view(array(
            'title'    => __('Not Found!'),
            'layout'   => '404.xml'
        ));
    }
}
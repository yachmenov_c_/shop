<?php

class AboutUs extends Controller {
    public function index() {
        $prod = new DBProducts();
        return $this->view(array(
            'title' => __('About Us'),
            'data'  => $prod->count()
        ));
    }
}
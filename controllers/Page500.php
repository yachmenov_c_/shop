<?php

class Page500 extends Controller {
    public function index() {
        return $this->view(array(
            'title'    => __('Something went wrong...'),
            'layout'   => '500.xml'
        ));
    }
}